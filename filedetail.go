package main

import (
	"errors"
	"net/http"
)

// FileDetail : used to put file information detail
type FileDetail struct {
	fileName     string
	fileSize     int64
	acceptRanges string
}

func getURLHeaders(url string) (headers http.Header, err error) {
	response, err := http.Head(url)
	if err != nil {
		err = errors.New("Error: Unable to download URL (" + url + ") with errors: " + err.Error())
		return nil, err
	}
	defer response.Body.Close()
	if response.StatusCode >= 300 {
		err = errors.New("Error: HTTP Status = " + response.Status)
		return nil, err
	}
	headers = response.Header
	return headers, err
}
