# Go Fetcher

This project is meant to be my learning purpose to develop golang with TDD.

To start using this tools, try to
- go build
- ./gofetcher -url=yourlink.tld/file.ext

Progress
- get header
- get file detail (all related for download)
- set worker
- fetch by part
- join the file

Box Idea
- Download in multiple connection.
- Pause / Resume Feature if available.
- Optimize connection and IO.