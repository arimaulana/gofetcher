package main

import (
	"flag"
	"fmt"
	"log"
)

var (
	usage         = "Usage: ./gofetcher -url=http://your.link/here.rar -c=10"
	link          *string
	maxConnection *int
)

func init() {
	link = flag.String("url", "", usage)
	maxConnection = flag.Int("c", 4, usage)
}

func main() {
	// parse the flag
	flag.Parse()

	// check url
	header, err := getURLHeaders(*link)
	if err != nil {
		log.Fatalln("Error: ", err)
	}
	fmt.Printf("File Header: %+v\n", header)
}
