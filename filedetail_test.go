package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetURLHeaders(t *testing.T) {
	testHandler := func(w http.ResponseWriter, req *http.Request) {
		// Before any call to WriteHeader or Write, declare
		// the trailers you will set during the HTTP
		// response. These three headers are actually sent in
		// the trailer.
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	}

	// create mock server with httptest
	ts := httptest.NewServer(http.HandlerFunc(testHandler))
	defer ts.Close()

	// get headers
	link := ts.URL
	Header, err := getURLHeaders(link)
	if err != nil {
		t.Errorf("Error occured: %v", err)
	}

	// check empty header or not
	if Header == nil {
		t.Errorf("Header not found!")
	}
}

func TestGetURLHeadersForBadURL(t *testing.T) {
	testHandler := func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	}

	// create mock server with httptest
	ts := httptest.NewServer(http.HandlerFunc(testHandler))
	defer ts.Close()

	// get headers
	link := ts.URL + "test" // expected URL with ip:porttest be error
	Header, err := getURLHeaders(link)
	if err == nil {
		t.Errorf("Expected an error to be raised")
	}

	// check empty header or not
	if Header != nil {
		t.Errorf("Header should be nil!")
	}
}

func TestGetURLHeadersForBadHTTPStatus(t *testing.T) {
	errorStatus := http.StatusBadGateway
	testHandler := func(w http.ResponseWriter, req *http.Request) {
		// Before any call to WriteHeader or Write, declare
		// the trailers you will set during the HTTP
		// response. These three headers are actually sent in
		// the trailer.
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.WriteHeader(errorStatus)
	}

	// create mock server with httptest
	ts := httptest.NewServer(http.HandlerFunc(testHandler))
	defer ts.Close()

	// get headers
	link := ts.URL
	Header, err := getURLHeaders(link)
	if err == nil {
		t.Errorf("Expected to have an error %v", errorStatus)
	}

	// check empty header or not
	if Header != nil {
		t.Errorf("Header should be nil!")
	}

	// check if using invalid url
	link += "/test"
	Header, err = getURLHeaders(link)
	if err == nil {
		t.Errorf("Expected to have an error %v", errorStatus)
	}
}
